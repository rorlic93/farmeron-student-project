package com.example.rastimir.farmeronexample.Activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import com.example.rastimir.farmeronexample.API.RetrofitManager;
import com.example.rastimir.farmeronexample.Adapters.AnimalRecyclerViewAdapter;
import com.example.rastimir.farmeronexample.Database.myDBHandler;
import com.example.rastimir.farmeronexample.Models.Animal;
import com.example.rastimir.farmeronexample.Models.Entity;
import com.example.rastimir.farmeronexample.Models.Event;
import com.example.rastimir.farmeronexample.Models.Location;
import com.example.rastimir.farmeronexample.Models.Task;
import com.example.rastimir.farmeronexample.R;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FetchingActivity extends AppCompatActivity {

    private AnimalRecyclerViewAdapter adapter;
    private AccountManager manager;
    private Account[] accounts;
    boolean flag = false;

    String authToken = "";

    private ProgressDialog mProgress;

    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetching);

        mProgress = ProgressDialog.show(this, "Progress Dialog", "Fetching data..."); //    Not showing ProgressDialog (?)

        final myDBHandler dbHandler = new myDBHandler(this, null, null, 1);
        final SQLiteDatabase db = dbHandler.getReadableDatabase();

        manager = AccountManager.get(getBaseContext());

        accounts = AccountManager.get(getBaseContext()).getAccounts();
        for(Account account : accounts){
            Log.i("ACCOUNT", account.toString());
            if(account.name.equalsIgnoreCase("test")){
                try {
                    authToken = manager.blockingGetAuthToken(account, "com.example.rastimir.farmeronexample", true);
                } catch (OperationCanceledException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (AuthenticatorException e) {
                    e.printStackTrace();
                }
                Log.i("ACCOUNT_TOKKEN", authToken);

            }
        }

        if(authToken != null){
            try {
                RetrofitManager.getEntitiesFromServer(authToken, new Callback<List<Entity>>() {
                    @Override
                    public void onResponse(Call<List<Entity>> call, Response<List<Entity>> response) {
                        if (response != null && response.isSuccessful()) {
                            String str = "";
                            List<Entity> entities = response.body();

                            for (Entity entity : entities) {
                                if (entity instanceof Animal) {
                                    Animal animal = new Animal(
                                            (((Animal) entity).getId()),
                                            ((Animal) entity).getLifeNumber(),
                                            ((Animal) entity).getRFID(),
                                            (((Animal) entity).getGeneralStatusId()),
                                            (((Animal) entity).getLocationId()),
                                            ((Animal) entity).getBirthDate()
                                    );
                                    dbHandler.addAnimal(animal);

                                    Log.i("ANIMAL", ((Animal) entity).getLifeNumber());
                                    str += "ID: " + ((Animal) entity).getId()
                                            + "\nLifeNumber: " + ((Animal) entity).getLifeNumber()
                                            + "\nRFID: " + ((Animal) entity).getRFID()
                                            + "\nGeneralStatusId: " + ((Animal) entity).getGeneralStatusId()
                                            + "\nLocationId: " + ((Animal) entity).getLocationId()
                                            + "\nBirthDate: " + ((Animal) entity).getBirthDate()
                                            + "\nEntityId: " + entity.getEntityId() + "\n\n";

                                }else if(entity instanceof Location){
                                    Location location = new Location(
                                            ((Location) entity).getId(),
                                            ((Location) entity).getName(),
                                            ((Location) entity).getCapacity(),
                                            ((Location) entity).getFeedingGroupID()
                                    );
                                    dbHandler.addLocation(location);
                                    Log.i("LOCATION", ((Location) entity).getName());

                                }else if(entity instanceof Event){
                                    Event event = new Event(
                                            ((Event) entity).getUUID(),
                                            ((Event) entity).getAnimalId(),
                                            ((Event) entity).getDate(),
                                            ((Event) entity).getComment(),
                                            ((Event) entity).getEventTypeId()
                                    );
                                    dbHandler.addEvent(event);
                                    Log.i("EVENT", ((Event) entity).getUUID());
                                }else if(entity instanceof Task){
                                    Task task = new Task(
                                            ((Task) entity).getId(),
                                            ((Task) entity).getAnimalId(),
                                            ((Task) entity).getDate(),
                                            ((Task) entity).getComment(),
                                            ((Task) entity).getEventTypeId()
                                    );
                                    dbHandler.addTask(task);
                                    Log.i("TASK", ((Task) entity).getDate());
                                }
                            }
                            Intent intent = new Intent(getBaseContext(), AnimalActivity.class);
                            if(mProgress.isShowing()){
                                mProgress.dismiss();
                            }
                            startActivity(intent);
                        } else if (response == null && !response.isSuccessful()) {

                        }
                    }

                    @Override
                    public void onFailure(Call<List<Entity>> call, Throwable t) {

                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }



}
