package com.example.rastimir.farmeronexample.Models;

import android.text.format.DateFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Rastimir on 25.10.2016..
 */
public class Animal extends Entity implements Serializable{

    private int Id;
    private String LifeNumber;
    private String RFID;
    private int GeneralStatusId;
    private int LocationId;
    private String BirthDate;

    public Animal(){

        EntityId = 1;
    }

    public Animal(int Id, String LifeNumber, String RFID, int GeneralStatusId, int LocationId, String BirthDate) {
        this.Id = Id;
        this.LifeNumber = LifeNumber;
        this.RFID = RFID;
        this.GeneralStatusId = GeneralStatusId;
        this.LocationId = LocationId;
        this.BirthDate = BirthDate;
        EntityId = 1;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getLifeNumber() {
        return LifeNumber;
    }

    public void setLifeNumber(String LifeNumber) {
        this.LifeNumber = LifeNumber;
    }

    public String getRFID() {
        return RFID;
    }

    public void setRFID(String RFID) {
        this.RFID = RFID;
    }

    public int getGeneralStatusId() {
        return GeneralStatusId;
    }

    public void setGeneralStatusId(int GeneralStatusId) {
        this.GeneralStatusId = GeneralStatusId;
    }

    public int getLocationId() {
        return LocationId;
    }

    public void setLocationId(int LocationId) {
        this.LocationId = LocationId;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String BirthDate) {
        this.BirthDate = BirthDate;
    }
}
