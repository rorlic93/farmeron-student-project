package com.example.rastimir.farmeronexample.Models;

/**
 * Created by Rastimir on 25.10.2016..
 */
public class Entity {
    public int EntityId;

    public int getEntityId() {
        return EntityId;
    }

    public void setEntityId(int entityId) {
        EntityId = entityId;
    }
}
