package com.example.rastimir.farmeronexample;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.rastimir.farmeronexample.API.RetrofitManager;
import com.example.rastimir.farmeronexample.Activities.AnimalActivity;
import com.example.rastimir.farmeronexample.Activities.DetailsActivity;
import com.example.rastimir.farmeronexample.Activities.LoginActivity;
import com.example.rastimir.farmeronexample.Database.myDBHandler;
import com.example.rastimir.farmeronexample.Models.Animal;
import com.example.rastimir.farmeronexample.Models.Entity;
import com.example.rastimir.farmeronexample.Models.Event;
import com.example.rastimir.farmeronexample.Models.Location;
import com.example.rastimir.farmeronexample.Models.Task;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button loginBtn = (Button) findViewById(R.id.loginBtn);
        final TextView textView = (TextView) findViewById(R.id.textView);
        Button recyclerBtn = (Button) findViewById(R.id.recyclerBtn);
        Button button = (Button) findViewById(R.id.button);


        final myDBHandler dbHandler = new myDBHandler(this, null, null, 1);
        final SQLiteDatabase db = dbHandler.getWritableDatabase();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    RetrofitManager.getEntitiesFromServer("12345", new Callback<List<Entity>>() {
                        @Override
                        public void onResponse(Call<List<Entity>> call, Response<List<Entity>> response) {
                            if (response != null && response.isSuccessful()) {
                                String str = "";
                                List<Entity> entities = response.body();

                                for (Entity entity : entities) {
                                    if (entity instanceof Animal) {
                                        Animal animal = new Animal(
                                                (((Animal) entity).getId()),
                                                ((Animal) entity).getLifeNumber(),
                                                ((Animal) entity).getRFID(),
                                                (((Animal) entity).getGeneralStatusId()),
                                                (((Animal) entity).getLocationId()),
                                                ((Animal) entity).getBirthDate()
                                        );
                                        dbHandler.addAnimal(animal);

                                        Log.i("ANIMAL", ((Animal) entity).getLifeNumber());
                                        str += "ID: " + ((Animal) entity).getId()
                                                + "\nLifeNumber: " + ((Animal) entity).getLifeNumber()
                                                + "\nRFID: " + ((Animal) entity).getRFID()
                                                + "\nGeneralStatusId: " + ((Animal) entity).getGeneralStatusId()
                                                + "\nLocationId: " + ((Animal) entity).getLocationId()
                                                + "\nBirthDate: " + ((Animal) entity).getBirthDate()
                                                + "\nEntityId: " + entity.getEntityId() + "\n\n";

                                    }else if(entity instanceof Location){
                                        Location location = new Location(
                                                ((Location) entity).getId(),
                                                ((Location) entity).getName(),
                                                ((Location) entity).getCapacity(),
                                                ((Location) entity).getFeedingGroupID()
                                        );
                                        dbHandler.addLocation(location);
                                        Log.i("LOCATION", ((Location) entity).getName());

                                    }else if(entity instanceof Event){
                                        Event event = new Event(
                                                ((Event) entity).getUUID(),
                                                ((Event) entity).getAnimalId(),
                                                ((Event) entity).getDate(),
                                                ((Event) entity).getComment(),
                                                ((Event) entity).getEventTypeId()
                                        );
                                        dbHandler.addEvent(event);
                                        Log.i("EVENT", ((Event) entity).getUUID());
                                    }else if(entity instanceof Task){
                                        Task task = new Task(
                                                ((Task) entity).getId(),
                                                ((Task) entity).getAnimalId(),
                                                ((Task) entity).getDate(),
                                                ((Task) entity).getComment(),
                                                ((Task) entity).getEventTypeId()
                                        );
                                        dbHandler.addTask(task);
                                        Log.i("TASK", ((Task) entity).getDate());
                                    }
                                }
                                textView.setText("Procitano iz API-a, spremljeno u bazu...");
                            } else if (response != null && response.isSuccessful()) {

                            }
                        }

                        @Override
                        public void onFailure(Call<List<Entity>> call, Throwable t) {

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(getBaseContext(), LoginActivity.class);
            startActivity(intent);
            }
        });

        recyclerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(getBaseContext(), AnimalActivity.class);
            startActivity(intent);
            }
        });




    }
}
