package com.example.rastimir.farmeronexample.API;

import com.example.rastimir.farmeronexample.Deserialize.JSONSerializer;
import com.example.rastimir.farmeronexample.Models.Entity;
import com.example.rastimir.farmeronexample.Models.User;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rastimir on 26.10.2016..
 */
public class RetrofitManager {
    private static Retrofit getRetrofit() {
        return new Retrofit.Builder().baseUrl("http://192.168.1.44:22336")
                .addConverterFactory(GsonConverterFactory
                .create(JSONSerializer.getGson()))
                .build();
    }

    public static void getEntitiesFromServer(String authKey, Callback<List<Entity>> callback) throws IOException {
        Retrofit retrofit = getRetrofit();
        ApiMethodServices service = retrofit.create(ApiMethodServices.class);
        Call<List<Entity>> call = service.getEntities(authKey);
        call.enqueue(callback);
    }

    public static void getAuthKey(User user, Callback<String> callback) throws IOException{
        Retrofit retrofit = getRetrofit();
        ApiMethodServices service = retrofit.create(ApiMethodServices.class);
        Call<String>call = service.doAuth(user);
        call.enqueue(callback);
    }
}
