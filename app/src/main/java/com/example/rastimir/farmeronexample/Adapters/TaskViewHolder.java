package com.example.rastimir.farmeronexample.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.rastimir.farmeronexample.R;

/**
 * Created by Rastimir on 03.11.2016..
 */
public class TaskViewHolder extends RecyclerView.ViewHolder {

    TextView taskEventTypeTxt;
    TextView taskDateTxt;

    public TaskViewHolder(View itemView) {
        super(itemView);

        this.taskEventTypeTxt = (TextView)itemView.findViewById(R.id.taskEventTypeTxt);
        this.taskDateTxt = (TextView)itemView.findViewById(R.id.taskDateTxt);

    }
}
