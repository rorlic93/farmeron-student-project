package com.example.rastimir.farmeronexample.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rastimir.farmeronexample.ENUM.EventType;
import com.example.rastimir.farmeronexample.Models.Task;
import com.example.rastimir.farmeronexample.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Rastimir on 03.11.2016..
 */
public class TaskRecyclerViewAdapter extends RecyclerView.Adapter<TaskViewHolder> {

    private List<Task> animalTasks;

    public TaskRecyclerViewAdapter(List<Task> animalTasks) {
        this.animalTasks = animalTasks;
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_view, parent, false);
        TaskViewHolder taskViewHolder = new TaskViewHolder(itemView);

        return taskViewHolder;
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        Task task = animalTasks.get(position);
        String taskDate = task.getDate();
        Date date = null;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            date = format.parse(taskDate);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        holder.taskEventTypeTxt.setText(EventType.getById(task.getEventTypeId()).getStringId());
        holder.taskDateTxt.setText(String.valueOf(date.getYear() + 1900) + "/" +
                String.valueOf(date.getMonth() + 1) + "/" +
                String.valueOf(date.getDate()));
    }

    @Override
    public int getItemCount() {
        return animalTasks.size();
    }
}
