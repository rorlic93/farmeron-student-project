package com.example.rastimir.farmeronexample.Models;

/**
 * Created by Rastimir on 25.10.2016..
 */
public class Event extends Entity {

    private String UUID;
    private int AnimalId;
    private String Date;
    private String Comment;
    private int EventTypeId;

    public Event(){
        EntityId = 3;
    }

    public Event(String UUID, int AnimalId, String Date, String Comment, int EventTypeId) {
        this.UUID = UUID;
        this.AnimalId = AnimalId;
        this.Date = Date;
        this.Comment = Comment;
        this.EventTypeId = EventTypeId;
        EntityId = 3;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public int getAnimalId() {
        return AnimalId;
    }

    public void setAnimalId(int AnimalId) {
        this.AnimalId = AnimalId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String Comment) {
        this.Comment = Comment;
    }

    public int getEventTypeId() {
        return EventTypeId;
    }

    public void setEventTypeId(int EventTypeId) {
        this.EventTypeId = EventTypeId;
    }
}
