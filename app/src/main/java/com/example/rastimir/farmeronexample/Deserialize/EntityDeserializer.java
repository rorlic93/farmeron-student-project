package com.example.rastimir.farmeronexample.Deserialize;

import com.example.rastimir.farmeronexample.Models.Animal;
import com.example.rastimir.farmeronexample.Models.Entity;
import com.example.rastimir.farmeronexample.Models.Event;
import com.example.rastimir.farmeronexample.Models.Location;
import com.example.rastimir.farmeronexample.Models.Task;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Rastimir on 26.10.2016..
 */
public class EntityDeserializer implements JsonDeserializer<Entity> {

    private static final String entityIdParamName = "EntityId";

    @Override
    public Entity deserialize(JsonElement jsonElement, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Entity entity = null;

        try {
            if (jsonElement.isJsonObject()) {
                JsonObject jsonObject = (JsonObject) jsonElement;
                int entityId = jsonObject.get(entityIdParamName).getAsInt();

                switch (entityId) {
                    case 1:
                        entity = context.deserialize(jsonObject, Animal.class);
                        break;
                    case 2:
                        entity = context.deserialize(jsonObject, Location.class);
                        break;
                    case 3:
                        entity = context.deserialize(jsonObject, Event.class);
                        break;
                    case 4:
                        entity = context.deserialize(jsonObject, Task.class);
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return entity;
    }
}