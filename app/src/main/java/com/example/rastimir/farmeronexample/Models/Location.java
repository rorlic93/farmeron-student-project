package com.example.rastimir.farmeronexample.Models;

/**
 * Created by Rastimir on 25.10.2016..
 */
public class Location extends Entity{
    private int Id;
    private String Name;
    private int Capacity;
    private int FeedingGroupId;

    public Location(){
        EntityId = 2;
    }

    public Location(int Id, String Name, int Capacity, int FeedingGroupId) {
        this.Id = Id;
        this.Name = Name;
        this.Capacity = Capacity;
        this.FeedingGroupId = FeedingGroupId;
        EntityId = 2;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getCapacity() {
        return Capacity;
    }

    public void setCapacity(int Capacity) {
        this.Capacity = Capacity;
    }

    public int getFeedingGroupID() {
        return FeedingGroupId;
    }

    public void setFeedingGroupID(int FeedingGroupId) {
        this.FeedingGroupId = FeedingGroupId;
    }
}
