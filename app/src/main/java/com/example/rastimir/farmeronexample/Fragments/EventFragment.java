package com.example.rastimir.farmeronexample.Fragments;


import android.database.sqlite.SQLiteDatabase;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rastimir.farmeronexample.Activities.DetailsActivity;
import com.example.rastimir.farmeronexample.Adapters.EventRecyclerViewAdapter;
import com.example.rastimir.farmeronexample.Database.myDBHandler;
import com.example.rastimir.farmeronexample.Models.Event;
import com.example.rastimir.farmeronexample.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends Fragment {

    private EventRecyclerViewAdapter eventAdapter;

    public EventFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_event, container, false);

        eventAdapter = new EventRecyclerViewAdapter(((DetailsActivity)getActivity()).animalEvents);

        RecyclerView eventRecycler = (RecyclerView)v.findViewById(R.id.eventRecycler);
        eventRecycler.setHasFixedSize(true);
        eventRecycler.addItemDecoration(new HorizontalSpaceDecorator(30));
        eventRecycler.setAdapter(eventAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        eventRecycler.setLayoutManager(layoutManager);

        //  Dohvaćanje animalEvent liste za recyclerView iz DetailsActivity-a
        // ((DetailsActivity)getActivity()).animalEvents



        return v;
    }

}

class HorizontalSpaceDecorator extends RecyclerView.ItemDecoration{
    private final int spacer;

    public HorizontalSpaceDecorator(int spacer){
        this.spacer = spacer;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = spacer;
    }
}
