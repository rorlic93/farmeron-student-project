package com.example.rastimir.farmeronexample.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.rastimir.farmeronexample.R;

/**
 * Created by Rastimir on 29.10.2016..
 */
public class CustomViewHolder extends RecyclerView.ViewHolder {
    protected TextView AnimalTxt;
    protected TextView AnimalId;
    protected TextView AnimalLifeNumber;
    protected TextView AnimalRFID;

    public CustomViewHolder(View view){
        super(view);

        this.AnimalTxt = (TextView)view.findViewById(R.id.AnimalTxt);
        this.AnimalId = (TextView)view.findViewById(R.id.AnimalId);
        this.AnimalLifeNumber = (TextView)view.findViewById(R.id.AnimalLifeNumber);
        this.AnimalRFID = (TextView)view.findViewById(R.id.AnimalRFID);
    }

}
