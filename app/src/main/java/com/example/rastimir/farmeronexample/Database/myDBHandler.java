package com.example.rastimir.farmeronexample.Database;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import com.example.rastimir.farmeronexample.Models.Animal;
import com.example.rastimir.farmeronexample.Models.Event;
import com.example.rastimir.farmeronexample.Models.Location;
import com.example.rastimir.farmeronexample.Models.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rastimir on 28.10.2016..
 */
public class myDBHandler extends SQLiteOpenHelper{
    //BAZA
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "farmeron.db";

    //ANIMALS
    public static final String TABLE_ANIMALS = "Animals";
    public static final String ANIMALS_ID = "Id";
    public static final String ANIMALS_LIFE_NUMBER = "LifeNumber";
    public static final String ANIMALS_RFID = "RFID";
    public static final String ANIMALS_STATUS_ID = "GeneralStatusId";
    public static final String ANIMALS_LOCATION_ID = "LocationId";
    public static final String ANIMALS_BIRTH_DATE = "BirthDate";
    public static final String ANIMALS_ENTITY_ID = "EntityId";

    //LOCATIONS
    public static final String TABLE_LOCATIONS = "Locations";
    public static final String LOCATIONS_ID = "Id";
    public static final String LOCATIONS_NAME = "Name";
    public static final String LOCATIONS_CAPACITY = "Capacity";
    public static final String LOCATIONS_FEEDING_GROUP_ID = "FeedingGroupId";
    public static final String LOCATIONS_ENTITY_ID = "EntityId";

    //EVENTS
    public static final String TABLE_EVENTS = "Events";
    public static final String EVENTS_UUID = "UUID";
    public static final String EVENTS_ANIMAL_ID = "AnimalId";
    public static final String EVENTS_DATE = "Date";
    public static final String EVENTS_COMMENT = "Comment";
    public static final String EVENTS_TYPE_ID = "EventTypeId";
    public static final String EVENTS_ENTITY_ID = "EntityId";

    //TASKS
    public static final String TABLE_TASKS = "Tasks";
    public static final String TASKS_ID = "Id";
    public static final String TASKS_ANIMAL_ID = "AnimalId";
    public static final String TASKS_DATE = "Date";
    public static final String TASKS_COMMENT = "Comment";
    public static final String TASKS_TYPE_ID = "EventTypeId";
    public static final String TASKS_ENTITY_ID = "EntityId";

    public myDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        
        String query_A = "CREATE TABLE " + TABLE_ANIMALS + "(" +
                ANIMALS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ANIMALS_LIFE_NUMBER + " TEXT, " +
                ANIMALS_RFID + " TEXT, " +
                ANIMALS_STATUS_ID + " INTEGER, " +
                ANIMALS_LOCATION_ID + " INTEGER, " +
                ANIMALS_BIRTH_DATE + " TEXT, " +
                ANIMALS_ENTITY_ID + " INTEGER " +
                //"FOREIGN KY (" + ANIMALS_LOCATION_ID + ") REFERENCES " + TABLE_LOCATIONS + "(" + LOCATIONS_ID + ")" +
                ");";
        db.execSQL(query_A);

        String query_L = "CREATE TABLE " + TABLE_LOCATIONS + " (" +
                LOCATIONS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                LOCATIONS_NAME + " TEXT, " +
                LOCATIONS_CAPACITY + " INTEGER, " +
                LOCATIONS_FEEDING_GROUP_ID + " INTEGER, " +
                LOCATIONS_ENTITY_ID + " INTEGER" +
                ");";
        db.execSQL(query_L);

        String query_E = "CREATE TABLE " + TABLE_EVENTS + " (" +
                EVENTS_UUID + " TEXT, " +
                EVENTS_ANIMAL_ID + " INTEGER, " +
                EVENTS_DATE + " TEXT, " +
                EVENTS_COMMENT + " TEXT, " +
                EVENTS_TYPE_ID + " INTEGER, " +
                EVENTS_ENTITY_ID + " INTEGER " +
                ");";
        db.execSQL(query_E);

        String query_T = "CREATE TABLE " + TABLE_TASKS + " (" +
                TASKS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TASKS_ANIMAL_ID + " INTEGER, " +
                TASKS_DATE + " TEXT, " +
                TASKS_COMMENT + " TEXT, " +
                TASKS_TYPE_ID + " INTEGER, " +
                TASKS_ENTITY_ID + " INTEGER " +
                ");";
        db.execSQL(query_T);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ANIMALS);
        onCreate(db);
    }
    //ADDING TO DATABASE...
    //ANIMAL
    public void addAnimal(Animal animal){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ANIMALS_LIFE_NUMBER, animal.getLifeNumber());
        contentValues.put(ANIMALS_RFID, animal.getRFID());
        contentValues.put(ANIMALS_STATUS_ID, animal.getGeneralStatusId());
        contentValues.put(ANIMALS_LOCATION_ID, animal.getLocationId());
        contentValues.put(ANIMALS_BIRTH_DATE, animal.getBirthDate());
        contentValues.put(ANIMALS_ENTITY_ID, animal.getEntityId());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_ANIMALS, null, contentValues);
        db.close();
    }

    //LOCATION
    public void addLocation(Location location){
        ContentValues contentValues = new ContentValues();
        contentValues.put(LOCATIONS_NAME, location.getName());
        contentValues.put(LOCATIONS_CAPACITY, location.getCapacity());
        contentValues.put(LOCATIONS_FEEDING_GROUP_ID, location.getFeedingGroupID());
        contentValues.put(LOCATIONS_ENTITY_ID, location.getEntityId());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_LOCATIONS, null, contentValues);
        db.close();
    }

    //EVENT
    public void addEvent(Event event){
        ContentValues contentValues = new ContentValues();
        contentValues.put(EVENTS_UUID, event.getUUID());
        contentValues.put(EVENTS_ANIMAL_ID, event.getAnimalId());
        contentValues.put(EVENTS_DATE, event.getDate());
        contentValues.put(EVENTS_COMMENT, event.getComment());
        contentValues.put(EVENTS_TYPE_ID, event.getEventTypeId());
        contentValues.put(EVENTS_ENTITY_ID, event.getEntityId());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_EVENTS, null, contentValues);
        db.close();
    }

    //TASK
    public void addTask(Task task){
        ContentValues contentValues = new ContentValues();
        contentValues.put(TASKS_ANIMAL_ID, task.getAnimalId());
        contentValues.put(TASKS_DATE, task.getDate());
        contentValues.put(TASKS_COMMENT, task.getComment());
        contentValues.put(TASKS_TYPE_ID, task.getEventTypeId());
        contentValues.put(TASKS_ENTITY_ID, task.getEntityId());

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_TASKS, null, contentValues);
        db.close();
    }

    //READING FROM DATABASE
    //ANIMALS
    public List<Animal> listAll(SQLiteDatabase db){
        List<Animal> animalsList = new ArrayList<>();

        String selectAll = "SELECT * FROM " + TABLE_ANIMALS;
        Cursor cursor = db.rawQuery(selectAll, null);
        if(cursor.moveToFirst()){
            do{
                Animal animal = new Animal();
                animal.setId(Integer.parseInt(cursor.getString(0)));
                animal.setLifeNumber(cursor.getString(1));
                animal.setRFID(cursor.getString(2));
                animal.setGeneralStatusId(Integer.parseInt(cursor.getString(3)));
                animal.setLocationId(Integer.parseInt(cursor.getString(4)));
                animal.setBirthDate(cursor.getString(5));
                animal.setEntityId(Integer.parseInt(cursor.getString(6)));
                animalsList.add(animal);
            }while(cursor.moveToNext());
        }
        return animalsList;
    }

    //FIND ANIMAL WHERE ID = ID
    public Animal findAnimal(SQLiteDatabase db, String Id){
        Animal animal = new Animal();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_ANIMALS + " WHERE " + ANIMALS_ID + " = '" + Id.trim() + "'", null);
        if(cursor.moveToFirst()){
            animal.setId(Integer.parseInt(cursor.getString(0)));
            animal.setLifeNumber(cursor.getString(1));
            animal.setRFID(cursor.getString(2));
            animal.setGeneralStatusId(Integer.parseInt(cursor.getString(3)));
            animal.setLocationId(Integer.parseInt(cursor.getString(4)));
            animal.setBirthDate(cursor.getString(5));
            animal.setEntityId(Integer.parseInt(cursor.getString(6)));
        }

        return animal;
    }

    //SELECT * WHERE ANIMAL.LOCATIONID = LOCATION.ID
    public Location getAnimalLocation(SQLiteDatabase db, String Id){
        Animal animal = new Animal();
        Location animalLocation = new Location();

        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_ANIMALS + " WHERE " + ANIMALS_ID + " = " + Id, null);
        if(c.moveToFirst()){
            animal.setId(Integer.parseInt(c.getString(0)));
            animal.setLifeNumber(c.getString(1));
            animal.setRFID(c.getString(2));
            animal.setGeneralStatusId(Integer.parseInt(c.getString(3)));
            animal.setLocationId(Integer.parseInt(c.getString(4)));
            animal.setBirthDate(c.getString(5));
            animal.setEntityId(Integer.parseInt(c.getString(6)));
        }


        Cursor cLoc = db.rawQuery("SELECT * FROM " + TABLE_LOCATIONS + " WHERE " + LOCATIONS_ID + " = " + String.valueOf(animal.getLocationId()), null);
        if(cLoc.moveToFirst()){
            animalLocation.setId(Integer.parseInt(cLoc.getString(0)));
            animalLocation.setName(cLoc.getString(1));
            animalLocation.setCapacity(Integer.parseInt(cLoc.getString(2)));
            animalLocation.setFeedingGroupID(Integer.parseInt(cLoc.getString(3)));
        }
        return animalLocation;
    }

    //LIST ALL EVENTS WHERE ANIMALID = ID
    public List<Event> getAnimalEvents(SQLiteDatabase db, String Id){
        List<Event> animalEvents = new ArrayList<>();
        String OrderBy = "";

        Cursor result = db.rawQuery("SELECT * FROM " + TABLE_EVENTS + " WHERE " + EVENTS_ANIMAL_ID + " = '" + Id.trim() + "'",null);

        if(result.moveToFirst()){
            do{
                Event event = new Event();
                event.setUUID(result.getString(0));
                event.setAnimalId(Integer.parseInt(result.getString(1)));
                event.setDate(result.getString(2));
                event.setComment(result.getString(3));
                event.setEventTypeId(Integer.parseInt(result.getString(4)));
                animalEvents.add(event);
            }while(result.moveToNext());
        }
        return animalEvents;
    }


    //LIST ALL TASKS WHERE ANIMALID = ID
    public List<Task> getAnimalTasks(SQLiteDatabase db, String Id){
        List<Task> animalTasks = new ArrayList<>();

        Cursor cAnim = db.rawQuery("SELECT * FROM " + TABLE_TASKS + " WHERE " + TASKS_ANIMAL_ID + " = '" + Id.trim() + "'", null);
        if(cAnim.moveToFirst()){
            do{
                Task task = new Task();
                task.setId(Integer.parseInt(cAnim.getString(0)));
                task.setAnimalId(Integer.parseInt(cAnim.getString(1)));
                task.setDate(cAnim.getString(2));
                task.setComment(cAnim.getString(3));
                task.setEventTypeId(Integer.parseInt(cAnim.getString(4)));
                animalTasks.add(task);
            }while (cAnim.moveToNext());
        }
        return animalTasks;
    }

}
