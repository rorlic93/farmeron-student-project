package com.example.rastimir.farmeronexample.API;

import com.example.rastimir.farmeronexample.Models.Entity;
import com.example.rastimir.farmeronexample.Models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Matija on 10/28/2016.
 */

public interface ApiMethodServices {
    @GET("/all")
    Call<List<Entity>> getEntities(@Header("Authorization") String authKey);

    @POST("/authenticate")
    Call<String> doAuth(@Body User user);
}
