package com.example.rastimir.farmeronexample.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rastimir.farmeronexample.ENUM.EventType;
import com.example.rastimir.farmeronexample.Models.Event;
import com.example.rastimir.farmeronexample.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Rastimir on 02.11.2016..
 */
public class EventRecyclerViewAdapter extends RecyclerView.Adapter<EventViewHolder> {

    private List<Event> eventList;

    public EventRecyclerViewAdapter(List<Event> eventList) {
        this.eventList = eventList;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_view, parent, false);
        EventViewHolder eventViewHolder = new EventViewHolder(view);

        return eventViewHolder;
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        Event event = eventList.get(position);
        String eventDate = event.getDate().toString();
        Date date=null;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            date = format.parse(eventDate);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        holder.eventTypeTxt.setText(EventType.getById(event.getEventTypeId()).getStringId());
        holder.eventDateTxt.setText(String.valueOf(date.getYear() + 1900) + "/" +
                String.valueOf(date.getMonth() + 1) + "/" +
                String.valueOf(date.getDate()));


    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }
}
