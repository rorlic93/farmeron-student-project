package com.example.rastimir.farmeronexample.Activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Rect;
import android.os.RemoteException;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.rastimir.farmeronexample.API.RetrofitManager;
import com.example.rastimir.farmeronexample.AccountManager.Authenticator;
import com.example.rastimir.farmeronexample.Adapters.AnimalRecyclerViewAdapter;
import com.example.rastimir.farmeronexample.Database.myDBHandler;
import com.example.rastimir.farmeronexample.Models.Animal;
import com.example.rastimir.farmeronexample.Models.Entity;
import com.example.rastimir.farmeronexample.Models.Event;
import com.example.rastimir.farmeronexample.Models.Location;
import com.example.rastimir.farmeronexample.Models.Task;
import com.example.rastimir.farmeronexample.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnimalActivity extends AppCompatActivity {

    private AnimalRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        //DB components
        final myDBHandler dbHandler = new myDBHandler(this, null, null, 1);
        final SQLiteDatabase db = dbHandler.getReadableDatabase();

        //Reading from DB
        List<Animal> animalList;
        animalList = dbHandler.listAll(db);

        adapter = new AnimalRecyclerViewAdapter(AnimalActivity.this, animalList);

        //Recyclerview implementation...
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.animalList);
        recyclerView.setHasFixedSize(true);
        //Adding space between items
        recyclerView.addItemDecoration(new HorizontalSpaceDecorator(30));
        recyclerView.setAdapter(adapter);

        //Calling LinearLayoutManager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);

    }
}

class HorizontalSpaceDecorator extends RecyclerView.ItemDecoration{
    private final int spacer;

    public HorizontalSpaceDecorator(int spacer){
        this.spacer = spacer;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = spacer;
    }
}


