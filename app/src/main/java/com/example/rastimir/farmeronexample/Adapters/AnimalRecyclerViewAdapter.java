package com.example.rastimir.farmeronexample.Adapters;

import com.example.rastimir.farmeronexample.Activities.AnimalActivity;
import com.example.rastimir.farmeronexample.Activities.DetailsActivity;
import com.example.rastimir.farmeronexample.Adapters.CustomViewHolder;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rastimir.farmeronexample.Models.Animal;
import com.example.rastimir.farmeronexample.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rastimir on 28.10.2016..
 */

public class AnimalRecyclerViewAdapter extends RecyclerView.Adapter<CustomViewHolder> {
    private Context context;

    private List<Animal> animalList;
    private AnimalActivity A;

    public AnimalRecyclerViewAdapter(Context context, List<Animal> animalList){
        this.animalList = animalList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.animal_view, viewGroup, false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, int position) {
        final Animal animal = animalList.get(position);

        customViewHolder.AnimalTxt.setText(String.valueOf(animal.getGeneralStatusId()));
        customViewHolder.AnimalId.setText(String.valueOf(animal.getId()));
        customViewHolder.AnimalLifeNumber.setText(animal.getLifeNumber());
        customViewHolder.AnimalRFID.setText(animal.getRFID());

        context = customViewHolder.itemView.getContext();

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("Id", String.valueOf(animal.getId()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return animalList.size();
    }



}
