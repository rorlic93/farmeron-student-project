package com.example.rastimir.farmeronexample.Activities;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import android.content.Intent;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rastimir.farmeronexample.API.RetrofitManager;
import com.example.rastimir.farmeronexample.Database.myDBHandler;
import com.example.rastimir.farmeronexample.MainActivity;
import com.example.rastimir.farmeronexample.Models.Animal;
import com.example.rastimir.farmeronexample.Models.Entity;
import com.example.rastimir.farmeronexample.Models.Event;
import com.example.rastimir.farmeronexample.Models.Location;
import com.example.rastimir.farmeronexample.Models.Task;
import com.example.rastimir.farmeronexample.Models.User;
import com.example.rastimir.farmeronexample.R;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Bind;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AccountAuthenticatorActivity {

    //private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @Bind(R.id.input_email) EditText emailTxt;
    @Bind(R.id.input_password) EditText passwordTxt;
    @Bind(R.id.btn_login) Button loginBtn;
    @Bind(R.id.newScreenBtn) Button newScreenBtn;
//    @Bind(R.id.bottomSheetBtn) Button bottomSheetBtn;

    //IMPLEMENTING AUTHENTICATOR_ACTIVITY . . .
    User user;
    final myDBHandler dbHandler = new myDBHandler(this, null, null, 1);

    //MyAsyncTask myAsyncTask;
    ProgressDialog progressDialog;

    public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
    public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    public final static String ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT";
    private boolean flag = false;

    public static final String KEY_ERROR_MESSAGE = "ERR_MSG";

    public final static String PARAM_USER_PASS = "USER_PASS";

    private final String TAG = this.getClass().getSimpleName();

    private AccountManager mAccountManager;
    private String mAuthTokenType;
    String authtoken; // this
    String password;

    String accountName;

    public Account findAccount(String accountName) {
        for (Account account : mAccountManager.getAccounts())
            if (TextUtils.equals(account.name, accountName) && TextUtils.equals(account.type, getString(R.string.auth_type))) {
                System.out.println("FOUND");
                return account;
            }
        return null;
    }
    // . . .  AUTHENTICATOR_ACTIVITY

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        //AUTHENTICATOR_ACTIVITY...
        mAccountManager = AccountManager.get(getBaseContext());

        //IF this is a first time adding, then this will be null
        accountName = getIntent().getStringExtra(ARG_ACCOUNT_NAME);
        mAuthTokenType = getIntent().getStringExtra(ARG_AUTH_TYPE);

        if(mAuthTokenType == null)
            mAuthTokenType = getString(R.string.auth_type);

        findAccount(accountName);

        // . . . AUTHENTICATOR_ACTIVITY
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = new User(((EditText)findViewById(R.id.input_email)).getText().toString().trim(),
                        ((EditText)findViewById(R.id.input_password)).getText().toString().trim());
                try {
                    RetrofitManager.getAuthKey(user, new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if(response != null && response.isSuccessful()){
                                String str = "";

                                if(response.body().toString().equals("User code or password is incorrect")){
                                    Toast.makeText(getBaseContext(), "User code or passwrod is incorrect", Toast.LENGTH_LONG).show();
                                }else{
                                    user.setTokken(response.body().toString());
                                    Log.d("USER", user.getTokken());

                                    userSignIn();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        newScreenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), NewScreenActivity.class);
                startActivity(intent);
            }
        });

//        bottomSheetBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getBaseContext(), BottomSheetActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    void userSignIn(){
        authtoken = user.getTokken();
        accountName = user.getUserName();
        password = user.getPassword();
        final String accountType = getIntent().getStringExtra(ARG_ACCOUNT_TYPE);

        if(accountName.length() > 0){
            Bundle data = new Bundle();
            data.putString(AccountManager.KEY_ACCOUNT_NAME, accountName);
            data.putString(AccountManager.KEY_ACCOUNT_TYPE, mAuthTokenType);
            data.putString(AccountManager.KEY_AUTHTOKEN, authtoken);
            data.putString(PARAM_USER_PASS, password);

            Bundle userData = new Bundle();
            userData.putString("UserID", "25");
            data.putBundle(AccountManager.KEY_USERDATA, userData);
            final Intent res = new Intent();
            res.putExtras(data);

            final Account account = new Account(accountName, mAuthTokenType);

            if(mAccountManager.addAccountExplicitly(account, password, userData)){
                Log.d(TAG, "Account added");
                mAccountManager.setAuthToken(account, mAuthTokenType, authtoken);
            }else{
                Log.d(TAG, "Account not added");
            }
            onLoginSuccess();
        }
    }

    public void login(){
        Log.d(TAG, "Login...");

        loginBtn.setEnabled(false);

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Fetching Data...");
        progressDialog.show();

        try {
            RetrofitManager.getEntitiesFromServer(user.getTokken().toString(), new Callback<List<Entity>>() {
                @Override
                public void onResponse(Call<List<Entity>> call, Response<List<Entity>> response) {
                    if (response != null && response.isSuccessful()) {
                        String str = "";
                        List<Entity> entities = response.body();

                        for (Entity entity : entities) {
                            if (entity instanceof Animal) {
                                Animal animal = new Animal(
                                        (((Animal) entity).getId()),
                                        ((Animal) entity).getLifeNumber(),
                                        ((Animal) entity).getRFID(),
                                        (((Animal) entity).getGeneralStatusId()),
                                        (((Animal) entity).getLocationId()),
                                        ((Animal) entity).getBirthDate()
                                );
                                dbHandler.addAnimal(animal);

                                Log.i("ANIMAL", ((Animal) entity).getLifeNumber());
                                str += "ID: " + ((Animal) entity).getId()
                                        + "\nLifeNumber: " + ((Animal) entity).getLifeNumber()
                                        + "\nRFID: " + ((Animal) entity).getRFID()
                                        + "\nGeneralStatusId: " + ((Animal) entity).getGeneralStatusId()
                                        + "\nLocationId: " + ((Animal) entity).getLocationId()
                                        + "\nBirthDate: " + ((Animal) entity).getBirthDate()
                                        + "\nEntityId: " + entity.getEntityId() + "\n\n";

                            }else if(entity instanceof Location){
                                Location location = new Location(
                                        ((Location) entity).getId(),
                                        ((Location) entity).getName(),
                                        ((Location) entity).getCapacity(),
                                        ((Location) entity).getFeedingGroupID()
                                );
                                dbHandler.addLocation(location);
                                Log.i("LOCATION", ((Location) entity).getName());

                            }else if(entity instanceof Event){
                                Event event = new Event(
                                        ((Event) entity).getUUID(),
                                        ((Event) entity).getAnimalId(),
                                        ((Event) entity).getDate(),
                                        ((Event) entity).getComment(),
                                        ((Event) entity).getEventTypeId()
                                );
                                dbHandler.addEvent(event);
                                Log.i("EVENT", ((Event) entity).getUUID());
                            }else if(entity instanceof Task){
                                Task task = new Task(
                                        ((Task) entity).getId(),
                                        ((Task) entity).getAnimalId(),
                                        ((Task) entity).getDate(),
                                        ((Task) entity).getComment(),
                                        ((Task) entity).getEventTypeId()
                                );
                                dbHandler.addTask(task);
                                Log.i("TASK", ((Task) entity).getDate());
                                onLoginSuccess();
                                //flag = true;
                            }
                        }
                    } else if (response == null && !response.isSuccessful()) {
                        onLoginFailed();
                        //flag = false;
                    }
                }

                @Override
                public void onFailure(Call<List<Entity>> call, Throwable t) {

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        finish();
    }

    public void onLoginSuccess(){
        Intent intent = new Intent(this, FetchingActivity.class);
        startActivity(intent);
    }

    public void onLoginFailed(){
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        progressDialog.dismiss();
        loginBtn.setEnabled(true);
    }
}




