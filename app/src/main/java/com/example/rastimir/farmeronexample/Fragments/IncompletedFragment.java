package com.example.rastimir.farmeronexample.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rastimir.farmeronexample.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncompletedFragment extends Fragment {


    public IncompletedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_incompleted, container, false);


        return v;
    }

}
