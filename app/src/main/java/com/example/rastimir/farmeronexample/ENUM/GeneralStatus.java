package com.example.rastimir.farmeronexample.ENUM;

import com.example.rastimir.farmeronexample.R;

/**
 * Created by Rastimir on 03.11.2016..
 */
public enum GeneralStatus {
    MILK(1, R.string.animal_generalStatuses_cow),
    CALF_F(2, R.string.animal_generalStatuses_calf_F_),
    HEIFER(3, R.string.animal_generalStatuses_heifer),
    DRY_COW(4, R.string.animal_generalStatuses_cowD),
    BULL(5, R.string.animal_generalStatuses_bull),
    CALF_M(8, R.string.animal_generalStatuses_calf_M_),
    DRY_HEIFER(9, R.string.animal_generalStatuses_heiferD),
    STEER_MEAT(10, R.string.animal_generalStatuses_steersMeat),
    BULL_MEAT(11, R.string.animal_generalStatuses_bullMeat);

    int id;
    int stringId;

    public static GeneralStatus getById(int id) {
        for(GeneralStatus e : values()) {
            if(e.id == id) return e;

        }
        return null;
    }

    GeneralStatus(int id, int animal_generalStatuses_cow) {
        this.id = id;
        this.stringId = animal_generalStatuses_cow;
    }

    public int getStringId() {
        return stringId;
    }

    public int getId() {
        return id;
    }
}
