package com.example.rastimir.farmeronexample.Activities;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rastimir.farmeronexample.Adapters.ViewPagerAdapter;
import com.example.rastimir.farmeronexample.Database.myDBHandler;
import com.example.rastimir.farmeronexample.ENUM.EventType;
import com.example.rastimir.farmeronexample.ENUM.GeneralStatus;
import com.example.rastimir.farmeronexample.Fragments.EventFragment;
import com.example.rastimir.farmeronexample.Fragments.TaskFragment;
import com.example.rastimir.farmeronexample.Models.Animal;
import com.example.rastimir.farmeronexample.Models.Event;
import com.example.rastimir.farmeronexample.Models.Location;
import com.example.rastimir.farmeronexample.Models.Task;
import com.example.rastimir.farmeronexample.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    private String Id;
    private Animal animal;
    public List<Event> animalEvents;
    private Location animalLocation;
    public List<Task> animalTasks;

    private TextView lifeNumberTxt;
    private TextView rfidTxt;
    private TextView birthDateTxt;
    private TextView generalStatusTxt;
    private TextView locationTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        final myDBHandler dbHandler = new myDBHandler(this, null, null, 1);
        final SQLiteDatabase db = dbHandler.getReadableDatabase();

        toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();
        Id = extras.getString("Id");

        //READING DATA FROM DB
        animal = dbHandler.findAnimal(db, Id);
        animalEvents = dbHandler.getAnimalEvents(db, Id);
        animalLocation = dbHandler.getAnimalLocation(db, Id);
        animalTasks = dbHandler.getAnimalTasks(db, Id);

        Log.i("ANIMALEVENTS", animalEvents.toString());

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager)findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new EventFragment(), "Event");
        viewPagerAdapter.addFragments(new TaskFragment(), "Task");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        lifeNumberTxt = (TextView)findViewById(R.id.lifeNumberTxt);
        rfidTxt = (TextView)findViewById(R.id.rfidTxt);
        birthDateTxt = (TextView)findViewById(R.id.birthDateTxt);
        generalStatusTxt = (TextView)findViewById(R.id.generalStatusTxt);
        locationTxt = (TextView)findViewById(R.id.locationTxt);

        lifeNumberTxt.setText("Life number: " + String.valueOf(animal.getLifeNumber()));
        rfidTxt.setText("RFID: " + animal.getRFID());

        Date date=null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            date = format.parse(animal.getBirthDate());
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        birthDateTxt.setText("Birth date: " + String.valueOf(date.getYear() + 1900) + "/" +
                String.valueOf(date.getMonth() + 1) + "/" +
                String.valueOf(date.getDate()));

        generalStatusTxt.setText(GeneralStatus.getById(animal.getGeneralStatusId()).getStringId());
        locationTxt.setText(String.valueOf(animalLocation.getName()));




    }


}
