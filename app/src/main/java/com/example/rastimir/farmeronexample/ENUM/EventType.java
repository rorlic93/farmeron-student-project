package com.example.rastimir.farmeronexample.ENUM;

import com.example.rastimir.farmeronexample.R;

/**
 * Created by Rastimir on 03.11.2016..
 */
public enum EventType {
    BIRTH(1, R.string.events_types__cattle_birth),
    GENERAL_STATUS_CHANGED(2, R.string.events_types_cattle_status_changed),
    HEALTH_CHECK(3, R.string.events_types_veterinary),
    CALVING(4, R.string.events_types_cattle_calving),
    MIGRATION(5, R.string.events_types_object_changed),
    QUARANTINE_START(6, R.string.events_types_cattle_quarantine_start),
    QUARANTINE_END(7, R.string.events_types_cattle_quarantine_end),
    WEIGHING(8, R.string.events_types_cattle_weighing),
    HOOF_CHECK(9, R.string.events_types_hoofCheck),
    HEAT(10, R.string.events_types_heat),
    SYNC_ACTION(11, R.string.events_types_prostaglandin_treatment),
    PREG_CHECK(12, R.string.events_types_pregnancy_check),
    NOT_SEEN_IN_HEAT(13, R.string.events_types_not_seen_in_heat),
    DO_NOT_BREED(14, R.string.events_types_do_not_breed),
    ABORTION(15, R.string.events_types_abortion),
    VACCINATION(16, R.string.events_types_vaccination),
    DRY_OFF(17, R.string.events_types_dry_off),
    CULL(18, R.string.events_types_cull),
    INSEMINATION(19, R.string.events_types_cattle_insermination),
    DEATH(20, R.string.events_types_cattle_death),
    REPRODUCTIVE_HEALTH_CHECK(21, R.string.events_types_reproductive_health_check),
    HOOF_TREATMENT(22, R.string.events_types_hoof_check_treatment)
    ;

    int Id;
    int stringId;

    EventType(int Id, int stringId) {
        this.Id = Id;
        this.stringId = stringId;
    }

    public static EventType getById(int id) {
        for(EventType e : values()) {
            if(e.Id == id) return e;

        }
        return null;
    }

    public int getId() {
        return Id;
    }

    public int getStringId() {
        return stringId;
    }
}
