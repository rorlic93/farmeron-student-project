package com.example.rastimir.farmeronexample.ENUM;

import com.example.rastimir.farmeronexample.R;

/**
 * Created by Rastimir on 03.11.2016..
 */
public enum FeedingGroup {
    B(1, R.string.feedingGroups_B),
    BEEF_CATTLE(2, R.string.feedingGroups_BC),
    B_MEAT(3, R.string.feedingGroups_Bmeat),
    C_MLIK(4, R.string.feedingGroups_milk),
    H_DRY(5, R.string.feedingGroups_Dh);

    int Id;
    int stringId;


    FeedingGroup(int Id, int stringId) {
        this.Id = Id;
        this.stringId = stringId;
    }

    public int getId() {
        return Id;
    }

    public int getStringId() {
        return stringId;
    }
}
