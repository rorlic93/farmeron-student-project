package com.example.rastimir.farmeronexample.Fragments;


import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rastimir.farmeronexample.Activities.DetailsActivity;
import com.example.rastimir.farmeronexample.Adapters.TaskRecyclerViewAdapter;
import com.example.rastimir.farmeronexample.Adapters.TaskViewHolder;
import com.example.rastimir.farmeronexample.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskFragment extends Fragment {
    private TaskRecyclerViewAdapter taskAdapter;

    public TaskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_task, container, false);

        taskAdapter = new TaskRecyclerViewAdapter(((DetailsActivity)getActivity()).animalTasks);

        RecyclerView taskRecycler = (RecyclerView)v.findViewById(R.id.taskRecycler);
        taskRecycler.setHasFixedSize(true);
        taskRecycler.addItemDecoration(new HorizontalSpaceDecorator(30));
        taskRecycler.setAdapter(taskAdapter);

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setOrientation(LinearLayoutManager.VERTICAL);

        taskRecycler.setLayoutManager(lm);


        return v;
    }

}

