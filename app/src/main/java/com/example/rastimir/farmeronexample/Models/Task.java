package com.example.rastimir.farmeronexample.Models;
/**
 * Created by Rastimir on 25.10.2016..
 */
public class Task extends Entity{

    private int Id;
    private int AnimalId;
    private String Date;
    private String Comment;
    private int EventTypeId;

    public Task(){
        EntityId = 4;
    }

    public Task(int Id, int AnimalId, String date, String Comment, int EventTypeId) {
        this.Id = Id;
        this.AnimalId = AnimalId;
        Date = date;
        this.Comment = Comment;
        this.EventTypeId = EventTypeId;
        EntityId = 4;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getAnimalId() {
        return AnimalId;
    }

    public void setAnimalId(int AnimalId) {
        this.AnimalId = AnimalId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String Comment) {
        this.Comment = Comment;
    }

    public int getEventTypeId() {
        return EventTypeId;
    }

    public void setEventTypeId(int EventTypeId) {
        this.EventTypeId = EventTypeId;
    }
}
