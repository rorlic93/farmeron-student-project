package com.example.rastimir.farmeronexample.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.rastimir.farmeronexample.R;

/**
 * Created by Rastimir on 02.11.2016..
 */
public class EventViewHolder  extends RecyclerView.ViewHolder{

    TextView eventTypeTxt;
    TextView eventDateTxt;

    public EventViewHolder(View itemView) {
        super(itemView);

        this.eventTypeTxt = (TextView)itemView.findViewById(R.id.eventTypeTxt);
        this.eventDateTxt = (TextView)itemView.findViewById(R.id.eventDateTxt);
    }
}
