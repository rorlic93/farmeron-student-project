package com.example.rastimir.farmeronexample.Deserialize;

import com.example.rastimir.farmeronexample.Models.Entity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Rastimir on 26.10.2016..
 */
public class JSONSerializer {

    private static final Gson gson;

    static {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Entity.class, new EntityDeserializer());
        gson = builder.create();
    }

    public static Gson getGson() {
        return gson;
    }

    public static <T> T fromJson(String json, Class<T> classOfT){
        return fromJson(json, classOfT);
    }

}
