package com.example.rastimir.farmeronexample.Activities;

import java.lang.reflect.Field;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rastimir.farmeronexample.Adapters.ViewPagerAdapter;
import com.example.rastimir.farmeronexample.Fragments.CompletedFragment;
import com.example.rastimir.farmeronexample.Fragments.IncompletedFragment;
import com.example.rastimir.farmeronexample.Menu.IconizedMenu;
import com.example.rastimir.farmeronexample.R;

import java.lang.reflect.Method;
import java.text.Format;

public class NewScreenActivity extends AppCompatActivity {

    private TabLayout tabLayoutNewScreen;
    private ViewPager viewPagerNewScreen;
    private ViewPagerAdapter viewPagerAdapterNewScreen;
    TextView FilterTxt;
    IconizedMenu popup;

    private BottomSheetBehavior bottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarNewScreen);
        setSupportActionBar(toolbar);

        tabLayoutNewScreen = (TabLayout) findViewById(R.id.tabLayoutNewScreen);
        viewPagerNewScreen = (ViewPager) findViewById(R.id.viewPagerNewScreen);

        viewPagerAdapterNewScreen = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapterNewScreen.addFragments(new IncompletedFragment(), "Remaining");
        viewPagerAdapterNewScreen.addFragments(new CompletedFragment(), "Completed");
        //viewPagerAdapterNewScreen.addFragments(new TaskFragment(), "Task");
        viewPagerNewScreen.setAdapter(viewPagerAdapterNewScreen);
        tabLayoutNewScreen.setupWithViewPager(viewPagerNewScreen);

        //Disabling swipe on ViewPager
        viewPagerNewScreen.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        FilterTxt = (TextView) findViewById(R.id.filter);
        createPopup(FilterTxt);

        FilterTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(v);
            }
        });

        initBottomSheet();
        initBottomSheetListener();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void initBottomSheet(){
        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
        View peakView = findViewById(R.id.PendingLayout);
        bottomSheetBehavior.setPeekHeight(peakView.getHeight());
        peakView.requestLayout();
    }

    private void initBottomSheetListener(){

//        final RelativeLayout layout = (RelativeLayout)findViewById(R.id.PendingLayout);
//        ViewTreeObserver vto = layout.getViewTreeObserver();
//        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                LayoutHeight = layout.getHeight();
//            }
//        });
//        bottomSheetBehavior.setPeekHeight(LayoutHeight);

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch(newState){
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.e("Bottom Sheet Behaviour", "STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.e("Bottom Sheet Behaviour", "STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.e("Bottom Sheet Behaviour", "STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.e("Bottom Sheet Behaviour", "STATE_HIDDEN");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.e("Bottom Sheet Behaviour", "STATE_SETTLING");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void createPopup(View v) {
        popup = new IconizedMenu(this, v);

        Menu mainMenu = popup.getMenu();

        SubMenu locationMenu = mainMenu.addSubMenu(0, 0, Menu.NONE, "Locations");
        SubMenu fertilityMenu = mainMenu.addSubMenu(0,1, Menu.NONE, "Fertility Events");
        SubMenu productionMenu = mainMenu.addSubMenu(0,2, Menu.NONE, "Production Events");
        SubMenu healthMenu = mainMenu.addSubMenu(0, 3, Menu.NONE, "Health Events");
        SubMenu protocolMenu = mainMenu.addSubMenu(0, 4, Menu.NONE, "Protocols");
        SubMenu dateMenu = mainMenu.addSubMenu(0, 5, Menu.NONE, "Date Range");

        locationMenu.getItem().setIcon(getResources().getDrawable(R.drawable.ic_navigation_reminders));
        fertilityMenu.getItem().setIcon(getResources().getDrawable(R.drawable.ic_navigation_tasks_fertility));
        productionMenu.getItem().setIcon(getResources().getDrawable(R.drawable.ic_navigation_tasks_production));
        healthMenu.getItem().setIcon(getResources().getDrawable(R.drawable.ic_navigation_tasks_health));
        protocolMenu.getItem().setIcon(getResources().getDrawable(R.drawable.ic_navigation_scheduled_tasks));
        dateMenu.getItem().setIcon(getResources().getDrawable(R.drawable.ic_navigation_tasks_today));

        SubMenu animalLocationsSubMenu = locationMenu.addSubMenu(0, 0, Menu.NONE, "Animal Locations");
        SubMenu animalLocationBoxSubMenu =  animalLocationsSubMenu.addSubMenu(0, 1, Menu.NONE, "1");
        animalLocationsSubMenu.add(0, 2, Menu.NONE, "2").setCheckable(true);
        SubMenu holdingPenSubMenu = locationMenu.addSubMenu(0, 1, Menu.NONE, "Holding Pens");
        holdingPenSubMenu.add(0, 3, Menu.NONE, "3").setCheckable(true);
        holdingPenSubMenu.add(0, 4, Menu.NONE, "4").setCheckable(true);

        animalLocationBoxSubMenu.add(0, 5, Menu.NONE, "5").setCheckable(true);
        animalLocationsSubMenu.setHeaderIcon(getResources().getDrawable(R.drawable.ic_navigation_reminders));

    }

    public void showPopup(View v) {
        popup.show();
        popup.setOnMenuItemClickListener(new IconizedMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch(item.getItemId()){
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            Toast.makeText(getBaseContext(), "You clicked on item: " + String.valueOf(item.getItemId()), Toast.LENGTH_SHORT).show();
                            item.setChecked(!item.isChecked());
                            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
                            item.setActionView(new View(getApplicationContext()));
                            return false;
                        default:
                            return false;
                    }
                }
        });

    }
}
